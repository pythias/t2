package t2

import "gitlab.com/pythias/t1"

type T2 struct {
	t1.T1
}

func (t2 *T2) Say() {
	t2.Hello()
}
